<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cliente_lista', 'ClienteController@lista');
Route::get('/cliente_editar', 'ClienteController@editar');
Route::post('/cliente_editar', 'ClienteController@alterar');
Route::get('/cliente_inserir', 'ClienteController@novo');
Route::post('/cliente_inserir', 'ClienteController@inserir');

Route::get('/paciente_lista', 'PacienteController@lista');
Route::get('/paciente_editar', 'PacienteController@editar');
Route::post('/paciente_editar', 'PacienteController@editar');
Route::get('/paciente_inserir', 'PacienteController@inserir');
Route::post('/paciente_inserir', 'PacienteController@inserir');

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
