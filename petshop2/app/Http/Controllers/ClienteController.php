<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClienteController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function lista(Request $request) {
        $nome_pesquisa = $request->query('nome', '');
        
//        $clientes = DB::select("
//select * from clientes
//where nome like ?
//order by nome
//        ", ['%' . $nome_pesquisa . '%']);

//        $clientes = DB::table('clientes')
//                ->where('nome', 'like', '%' . $nome_pesquisa . '%')
//                ->orderBy('nome')
//                ->get();
        
        $clientes = \App\Cliente::where('nome', 'like', '%' . $nome_pesquisa . '%')
                ->orderBy('nome')
                ->get();
        
        return view('cliente_lista',
            [
                'nome_pesquisa' => $nome_pesquisa,
                'quantidade_por_extenso' => count($clientes),
                'clientes' => $clientes,
            ]);
    }
    
    public function editar(Request $request) {
        $codigo = $request->query('codigo');
        $cliente = \App\Cliente::find($codigo);
        
        return view('cliente_editar',
            [
                'cliente' => $cliente,
            ]);
    }
    
    public function alterar(Request $request) {
        $codigo = $request->query('codigo');
        $dados = $request->validate([
            'nome' => 'required|max:255',
            'telefone' => 'required|max:255',
        ]);

        $cliente = \App\Cliente::find($codigo);
        $cliente->nome = $dados['nome'];
        $cliente->telefone = $dados['telefone'];
        $cliente->save();
        
        return view('cliente_editar',
            [
                'cliente' => $cliente,
            ]);
    }
    
    public function novo() {
        return view('cliente_inserir');
    }
    
    public function inserir(\App\Http\Requests\ClienteInserir $clienteInserir) {
        $dados = $clienteInserir->validated();
        \App\Cliente::create($dados);
        Session::flash('success', 'Cliente inserido com sucesso!');
        return redirect('cliente_lista');
    }
    
}
