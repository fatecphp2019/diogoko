<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PacienteController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lista(Request $request) {
        $nome_pesquisa = $request->query('nome', '');
        
        $pacientes = \App\Paciente::where('nome', 'like', '%' . $nome_pesquisa . '%')
                ->orderBy('nome')
                ->get();
        
        return view('paciente_lista',
            [
                'nome_pesquisa' => $nome_pesquisa,
                'quantidade_por_extenso' => count($pacientes),
                'pacientes' => $pacientes,
            ]);
    }
    
    public function editar(Request $request) {
        $codigo = $request->query('codigo');
        $metodo = $request->method(); // GET/POST
        if ($metodo == 'POST') {
            /*
            $nome = $request->input('nome');
            $ano_nascimento = $request->input('ano_nascimento');
            
            $paciente = \App\Paciente::find($codigo);
            $paciente->nome = $nome;
            $paciente->ano_nascimento = $ano_nascimento;
            $paciente->save();
             */
            $paciente = \App\Paciente::find($codigo);
            $paciente->update($request->input());
        }
                
        $paciente = \App\Paciente::find($codigo);
        
        return view('paciente_editar',
            [
                'paciente' => $paciente,
                'clientes' => \App\Cliente::orderBy('nome')->get(),
            ]);
    }
    
    public function inserir(Request $request) {
        $metodo = $request->method(); // GET/POST
        if ($metodo == 'POST') {
            //$nome = $request->input('nome');
            //$ano_nascimento = $request->input('ano_nascimento');
            //$cliente_id = $request->input('cliente_id');
            //$dono = \App\Cliente::find($cliente_id);
            
            /*
            $novoPaciente = new \App\Paciente();
            $novoPaciente->nome = $nome;
            $novoPaciente->ano_nascimento = $ano_nascimento;
            $novoPaciente->cliente()->associate($dono);
            //$novoPaciente->cliente_id = $cliente_id;
            $novoPaciente->save();
             */
            
            //\App\Paciente::create(compact(
            //    'nome', 'ano_nascimento', 'cliente_id'
            //));
            
            //\App\Paciente::create($request->input());

            /*
            $cliente_id = $request->input('cliente_id');
            $dono = \App\Cliente::find($cliente_id);
            $novoPaciente = new \App\Paciente();
            $novoPaciente->nome = $nome;
            $novoPaciente->ano_nascimento = $ano_nascimento;
            $dono->pacientes()->save($novoPaciente);
             */
            
            $cliente_id = $request->input('cliente_id');
            $dono = \App\Cliente::find($cliente_id);
            $dono->pacientes()->create($request->input());
            
            //return redirect('paciente_lista');
        }
        
        return view('paciente_editar', [
            'paciente' => new \App\Paciente(),
            'clientes' => \App\Cliente::orderBy('nome')->get(),
        ]);
    }

    
    
}
