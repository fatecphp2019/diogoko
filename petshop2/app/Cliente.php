<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nome', 'telefone'];
    
    public function pacientes() {
        return $this->hasMany('App\Paciente');
    }
}
