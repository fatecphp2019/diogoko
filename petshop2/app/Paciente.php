<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = ['nome', 'ano_nascimento'];
    
    public function cliente() {
        return $this->belongsTo('App\Cliente');
    }
}
