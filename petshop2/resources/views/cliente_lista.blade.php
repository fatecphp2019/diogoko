@extends('layouts.app')

@section('content')
<div class="container">
    @if (Session::has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
        <?php Session::forget('success'); ?>
    </div>
    @endif
    
    <h2>Clientes</h2>

    <form>
        <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome"
                   value="{{ $nome_pesquisa }}"
                   class="form-control"/>
        </div>
        
        <input type="submit" value="Pesquisar"
               class="btn btn-primary"/>
    </form>

    <br>

    <p>Existem {{ $quantidade_por_extenso }} clientes cadastrados.</p>

    <br>

    <table class="table">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Telefone</th>
            </tr>
        </thead>
        
        <tbody>
            @foreach ($clientes as $c)
            <tr>
                <td>
                <a href="{{ url('cliente_editar?codigo=' . $c->id) }}">
                        {{ $c->nome }}
                    </a>
                </td>
                <td>{{ $c->telefone }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
