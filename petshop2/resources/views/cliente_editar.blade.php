@extends('layouts.app')

@section('content')
<div class="container">
        <h2>Editar cliente</h2>
        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form method="POST">
            @csrf
            <div class="form-group">
                <label>Nome</label>
                <input type="text" name="nome"
                       value="{{ $cliente->nome }}"
                       class="form-control"/>
            </div>
            
            <div class="form-group">
                <label>Telefone</label>
                <input type="text" name="telefone"
                       value="{{ $cliente->telefone }}"
                       class="form-control"/>
            </div>
            
            <input type="submit" value="Salvar" class="btn btn-primary"/>
        </form>
        
        <br>
        
        <h3>Pacientes associados</h3>
        
        <table class="table">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Ano nascimento</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($cliente->pacientes()->orderBy('nome')->get() as $c)
                <tr>
                    <td>
                    <a href="paciente_editar?codigo={{ $c->id }}">
                            {{ $c->nome }}
                        </a>
                    </td>
                    <td>{{ $c->ano_nascimento }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

</div>        
@endsection
