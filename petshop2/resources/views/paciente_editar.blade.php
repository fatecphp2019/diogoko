@extends('layouts.app')

@section('content')
<div class="container">
        <h2>Editar paciente</h2>
        
        <form method="POST">
            @csrf
            <div class="form-group">
                <label>Nome</label>
                <input type="text" name="nome"
                       value="{{ $paciente->nome }}"
                       class="form-control"/>
            </div>
            
            <div class="form-group">
                <label>Ano nascimento</label>
                <input type="text" name="ano_nascimento"
                       value="{{ $paciente->ano_nascimento }}"
                       class="form-control"/>
            </div>
            
            <div class="form-group">
                <label>Dono</label>
                
                <select name="cliente_id"
                        class="form-control">
                    @foreach ($clientes as $c)
                    <option
                        value="{{ $c->id }}"
                        @if ($paciente->id && $c->id == $paciente->cliente->id)
                            selected
                        @endif
                    >
                        {{ $c->nome }}
                    </option>
                    @endforeach
                </select>
            </div>
            
            <input type="submit" value="Salvar" class="btn btn-primary"/>
        </form>
</div>
@endsection
