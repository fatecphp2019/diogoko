@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Pacientes</h2>

    <form>
        <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome"
                   value="{{ $nome_pesquisa }}"
                   class="form-control"/>
        </div>
        <input type="submit" value="Pesquisar" class="btn btn-primary"/>
    </form>

    <br>

    <p>Existem {{ $quantidade_por_extenso }} pacientes cadastrados.</p>

    <br>

    <table class="table">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Ano nascimento</th>
                <th>Dono</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($pacientes as $c)
            <tr>
                <td>
                <a href="paciente_editar?codigo={{ $c->id }}">
                        {{ $c->nome }}
                    </a>
                </td>
                <td>{{ $c->ano_nascimento }}</td>
                <td>{{ $c->cliente->nome }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
