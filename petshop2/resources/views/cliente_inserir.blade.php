@extends('layouts.app')

@section('content')
<div class="container">
        <h2>Inserir cliente</h2>
        
        <form method="POST">
            @csrf
            <div class="form-group">
                <label>Nome</label>
                <input type="text" name="nome"
                       value=""
                       class="form-control @error('nome') is-invalid @enderror"/>
                @error('nome')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            
            <div class="form-group">
                <label>Telefone</label>
                <input type="text" name="telefone"
                       value=""
                       class="form-control @error('nome') is-invalid @enderror"/>
                @error('telefone')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            
            <input type="submit" value="Salvar" class="btn btn-primary"/>
        </form>
</div>
@endsection
