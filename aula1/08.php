<?php

// Mostre usando o comando echo o nome, a cidade e a idade de cada
// pessoa do array abaixo.

$pessoas = [
    ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
    ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
    ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
    ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
];

foreach ($pessoas as $p) {
    $idade = date('Y') - $p['nascimento'];
    $cidade = $p['cidade'];
    //echo "<p>Nome: {$p['nome']}, Cidade: $cidade, Idade: $idade</p>";
    echo 'Idade ' . (2019 - $p['nascimento']) . '<br>';
}
