<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>16</title>
</head>
<body>
    <?php

    // Mostre no navegador nome, cidade e idade das pessoas do array abaixo
    // cuja idade seja maior ou igual à idade digitada no formulário (se não
    // houver valor digitado, assuma 0).

    $pessoas = [
        ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
        ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
        ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
        ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
    ];
    
    if (isset($_REQUEST['idade_minima'])) {
        $pessoas_filtrado = [];
        foreach ($pessoas as $p) {
            $idade = date('Y') - $p['nascimento'];
            if ($idade >= $_REQUEST['idade_minima']) {
                $pessoas_filtrado[] = $p;
            }
        }
    } else {
        $pessoas_filtrado = $pessoas;
    }

    ?>

    <form>
        <div>
            <label>Idade mínima</label>
            <input type="text" name="idade_minima"/>
            <input type="submit" value="Filtrar"/>
        </div>
    </form>

    <?php foreach ($pessoas_filtrado as $p): ?>
    <p>Nome: <?= $p['nome'] ?>, Cidade: <?= $p['cidade'] ?>,
        Idade: <?= date('Y') - $p['nascimento'] ?></p>
    <?php endforeach; ?>
</body>
</html>