<?php

// Calcule a média das idades das pessoas do array abaixo.

$pessoas = [
    ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
    ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
    ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
    ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
];

function media($arr) {
    $soma = array_sum($arr);
    return $soma / count($arr);
}

function idade($nascimento) {
    return date('Y') - $nascimento;
}

$nascimentos = array_column($pessoas, 'nascimento');
var_dump($nascimentos);
echo '<br>';




$idades = [];
foreach ($nascimentos as $n) {
    $idades[] = idade($n);
    array_push($idades, $n);
}
var_dump($idades);
echo '<br>';


$idades = array_map('idade', $nascimentos);


echo media($idades);
