<?php

// Crie uma função numero_maximo() que receba dois parâmetros
// numéricos e retorne o maior número entre os dois.
function numero_maximo($a, $b) {
    if ($a < $b) {
        return $b;
    } else if ($a > $b) {
        return $a;
    } else {
        throw new Exception('Valores iguais');
        //return 'Valores iguais';
    }
    
    
    
    //return ($a < $b) ? $b : $a;
}

try {
    echo numero_maximo(10, 10);
} catch (Exception $ex) {
    echo 'Senhor, por favor não digite valores iguais! Obrigadu!';
}
