<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>15</title>
</head>
<body>
    <?php

    // Mostre no navegador nome, cidade e idade das pessoas do array abaixo.

    $pessoas = [
        ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
        ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
        ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
        ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
    ];

    ?>

    <?php foreach ($pessoas as $p) { ?>
    <p>Nome: <?= $p['nome'] ?>, Cidade: <?= $p['cidade'] ?>,
        Idade: <?= date('Y') - $p['nascimento'] ?></p>
    <?php } ?>
    

    <?php foreach ($pessoas as $p): ?>
    <p>Nome: <?= $p['nome'] ?>, Cidade: <?= $p['cidade'] ?>,
        Idade: <?= date('Y') - $p['nascimento'] ?></p>
    <?php endforeach; ?>
</body>
</html>