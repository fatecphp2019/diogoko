<?php

// Calcule e mostre quantas pessoas moram em cada cidade.

$pessoas = [
    ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
    ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
    ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
    ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
];

/*
$quantidades = [];
foreach ($pessoas as $p) {
    $cidade = $p['cidade'];
    if (isset($quantidades[$cidade])) {
        $quantidades[$cidade]++;
    } else {
        $quantidades[$cidade] = 1;
    }
}
*/

foreach ($pessoas as $p) {
    $cidade = $p['cidade'];
    $quantidades[$cidade] = ($quantidades[$cidade] ?? 0) + 1;
}


$cidades = array_column($pessoas, 'cidade');
$quantidades = array_count_values($cidades);


echo '<pre>';
//var_dump($cidades);
var_dump($quantidades);
echo '</pre>';

$quantidades = [
    'Rio Preto' => 2,
    'Mirassol' => 1,
    'Bady' => 1,
];
