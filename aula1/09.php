<?php

// Busque no array abaixo o nome da pessoa mais velha.

$pessoas = [
    ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
    ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
    ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
    ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
];

function encontrar_mais_velha($pessoas) {
    $mais_velha = null;
    foreach ($pessoas as $p) {
        if ($mais_velha == null) {
            $mais_velha = $p;
        } else if ($p['nascimento'] < $mais_velha['nascimento']) {
            $mais_velha = $p;
        }
    }
    
    return $mais_velha;
}

$mais_velha = encontrar_mais_velha($pessoas);
echo $mais_velha['nome'] . ' é o mais velho com '
        . (2019 - $mais_velha['nascimento']) . ' anos';

echo '<hr>';

echo '<pre>';

$min = min(array_column($pessoas,'nascimento', 'nome')); # menor valor em [nome => nascimento]
var_dump($min);

$nomes = array_column($pessoas,'nascimento', 'nome'); # retorna [nome => nascimento]
var_dump($nomes);
$nome = array_search($min, $nomes); # procura valor (menor nascimento) nos valores de [nome => nascimento] e retorna o indice (nome)
echo $nome;

echo '</pre>';

echo(array_search(5, [10, 2, 'teste' => 5]));