<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>14</title>
</head>
<body>
    <?php

    // A partir de uma lista de números separados por vírgula
    // digitado no formulário, calcule e mostre no navegador
    // o maior número desse conjunto.

    ?>
    <form method="POST">
        <div>
            <label>Números separados por vírgulas</label>
            <input type="text" name="numeros"/>
            <input type="submit" value="Calcular"/>
        </div>
        <div>
            <?php
            if (isset($_POST['numeros'])) {
                $numeros = explode(',', $_POST['numeros']);
                $maximo = max($numeros);
                
                ?>
                Número máximo? <?php echo $maximo; ?>
                <?php
            }
            ?>
        </div>
    </form>
</body>
</html>