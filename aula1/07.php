<?php

// Crie uma função media() que receba um parâmetro que é um
// array de números e retorne a média aritmética desse conjunto
// de números.
function media($arr) {
    $soma = 0;
    foreach ($arr as $elemento) {
        $soma += $elemento;
    }
    
    return $soma / count($arr);
}

function media2($arr) {
    $soma = array_sum($arr);
    return $soma / count($arr);
}

echo media2([10,20,22]);
