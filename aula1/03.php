<?php

// Crie uma função que verifique se uma pessoa nascida nesse ano é maior
// de idade (18 anos ou mais) e retorne true se for, ou false se não for. Ignore o mês do nascimento.

function maior_idade($nascimento) {
    $idade = date('Y') - $nascimento;

    if ($idade >= 18) {
        echo 'S';
    } else {
        echo 'N';
    }
}

function maior_idade2($nascimento) {
    $idade = date('Y') - $nascimento;

    /*
    if ($idade >= 18) {
        return true;
    } else {
        return false;
    }
    */
    
    //return $idade >= 18 ? true : false;
    
    return $idade >= 18;
}

// Use a função para mostrar S ou N para uma pessoa nascida em 1995
//maior_idade(1998);
if (maior_idade2(1998, 'diogo')) {
    echo 'Yes';
} else {
    echo 'No';
}
