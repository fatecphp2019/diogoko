<?php

$idade = 35;

// Verifique se uma pessoa dessa idade é maior de idade (18 anos ou mais)
// e imprima S se for, ou N se não for.
if ($idade >= 18) {
    echo 'S';
} else {
    echo 'N';
}
