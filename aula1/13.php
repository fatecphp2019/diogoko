<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>13</title>
</head>
<body>
    <?php

    // A partir do ano de nascimento digitado no formulário,
    // mostre no navegador se a pessoa é maior de idade.
    $nascimento = filter_input(INPUT_POST, 'nascimento', FILTER_VALIDATE_INT);
    if ($nascimento === false) {
        echo '<p>Valor inválido!</p>';
    } else if ($nascimento != false && $nascimento != null) {
        $idade = date('Y') - $nascimento;
        if ($idade >= 18) {
            $maior = 'S';
        } else {
            $maior = 'N';
        }
    }
    
    ?>
    <form method="POST">
        <div>
            <label>Ano de nascimento</label>
            <input type="text" name="nascimento"/>
            <input type="submit" value="Calcular"/>
        </div>

        <?php
        if (isset($maior)) {
            echo '<div>';
                echo 'Maior de idade? '; echo $maior;
            echo '</div>';
        }
        ?>

        <?php if (isset($maior)) { ?>
        <div>
            Maior de idade? <?php echo $maior; ?>
        </div>
        <?php } ?>
    </form>
</body>
</html>