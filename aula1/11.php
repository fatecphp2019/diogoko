<?php

// Mostre usando o comando echo o nome e a cidade de cada
// pessoa do array abaixo que não é de 'Rio Preto'.

$pessoas = [
    ['nome' => 'Thanos', 'cidade' => 'Rio Preto', 'nascimento' => 1955],
    ['nome' => 'Aranha', 'cidade' => 'Mirassol', 'nascimento' => 1996],
    ['nome' => 'Hulk', 'cidade' => 'Rio Preto', 'nascimento' => 1988],
    ['nome' => 'Thor', 'cidade' => 'Bady', 'nascimento' => 1979],
];

foreach ($pessoas as $p) {
    if ($p['cidade'] != 'Rio Preto') {
        echo "<p>{$p['nome']} mora em {$p['cidade']}</p>";
    }
}
