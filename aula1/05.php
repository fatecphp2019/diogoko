<?php

// Crie uma função numero_maximo_array() que receba um parâmetro
// que é um array de números e retorna o maior número desse conjunto.
function numero_maximo_array($a) {
    $maximo = 0;
    
    for ($i = 0; $i < count($a); $i++) {
        if ($a[$i] > $maximo) {
            $maximo = $a[$i];
        }
    }

    for ($i = 0, $tamanhoA = count($a); $i < $tamanhoA; $i++) {
        if ($a[$i] > $maximo) {
            $maximo = $a[$i];
        }
    }
    
    foreach ($a as $i => $elemento) {
        if ($elemento > $maximo) {
            $maximo = $elemento;
        }
    }
    
    return $maximo;
}

function numero_maximo_array2($a) {
    return max($a);
}

echo numero_maximo_array2([10, 20, 30, 5]);
