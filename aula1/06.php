<?php

// Crie uma função media() que receba dois parâmetros
// numéricos e retorne a média aritmética dos dois números.
function media($a, $b) {
    return ($a + $b) / 2;
}

echo media(10, 32);
