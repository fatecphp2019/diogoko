<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>exercício aula 2</title>
</head>
<body>
<?php

// Mostre os nomes e salários das pessoas do departamento selecionado
// e os salários mínimo, medio e máximo do departamento.

$pessoas = [
    ['nome' => 'Thanos', 'departamento' => 'Compras', 'salario' => 2500],
    ['nome' => 'Aranha', 'departamento' => 'Administrativo', 'salario' => 2100],
    ['nome' => 'Hulk', 'departamento' => 'Compras', 'salario' => 1050],
    ['nome' => 'Thor', 'departamento' => 'Suporte', 'salario' => 2600],
];

function filtrar($pessoas, $departamento_selecionado) {
    $pessoas_filtrado = [];
    foreach ($pessoas as $p) {
        if ($p['departamento'] == $departamento_selecionado) {
            $pessoas_filtrado[] = $p;
        }
    }
    
    return $pessoas_filtrado;
}

function filtrar2_aux($p) {
    return ($p['departamento'] == $_REQUEST['departamento']);
    /*
    if ($p['departamento'] == $_REQUEST['departamento']) {
        return true;
    } else {
        return false;
    }
     */
}

function filtrar2($pessoas, $departamento_selecionado) {
    return array_filter($pessoas, 'filtrar2_aux');
}

function menor_salario($pessoas) {
    return min(array_column($pessoas, 'salario'));
}

function maior_salario($pessoas) {
    return max(array_column($pessoas, 'salario'));
}

function media_salario($pessoas) {
    return array_sum(array_column($pessoas, 'salario')) / count($pessoas);
}


$departamento_selecionado = $_REQUEST['departamento'] ?? 'Administrativo';
$pessoas_filtrado = filtrar2($pessoas, $departamento_selecionado);
$menor = menor_salario($pessoas_filtrado);
$maior = maior_salario($pessoas_filtrado);
$media = media_salario($pessoas_filtrado);

?>
    
    <form method="GET">
        <select name="departamento">
            <option value="Administrativo"
                    <?= ($departamento_selecionado == 'Administrativo') ?
                        'selected' : '' ?>
            >
                Administrativo
            </option>
            <option value="Compras"
                    <?= ($departamento_selecionado == 'Compras') ?
                        'selected' : '' ?>>
                Compras
            </option>
            <option value="Suporte"
                    <?= ($departamento_selecionado == 'Suporte') ?
                        'selected' : '' ?>>Suporte</option>
        </select>
        
        <input type="submit" value="Pesquisar"/>
    </form>
    
    <?php foreach ($pessoas_filtrado as $p): ?>
    <p><?= $p['nome'] ?> trabalha no departamento <?= $p['departamento'] ?> e
    recebe <?= $p['salario'] ?></p>
    <?php endforeach; ?>
    
    <p>Menor salário do departamento: <?= $menor ?></p>
    <p>Média de salário do departamento: <?= $media ?></p>
    <p>Maior salário do departamento: <?= $maior ?></p>
</body>
</html>