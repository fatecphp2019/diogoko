<?php

if (empty($_SESSION['usuario_logado'])) {
    die('Acesso negado');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // requisição POST, salva os dados e depois
    // carrega para mostrar o resultado
    $dados_novos = [
        'codigo' => $_REQUEST['codigo'],
        'nome' => $_REQUEST['nome'],
        'telefone' => $_REQUEST['telefone']
    ];
    $repoClientes->alterar($dados_novos);
}

// sempre carrega dados para mostrar
$cliente = $repoClientes->buscar_por_codigo($_REQUEST['codigo']);

require_once __DIR__ . '/../interface/pagina_cliente_editar.php';
