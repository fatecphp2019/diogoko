<?php

use \phputil\extenso\Extenso;

$nome_pesquisa = $_REQUEST['nome'] ?? '';
$clientes = $repoClientes->listar_clientes($nome_pesquisa);

$e = new Extenso();
$quantidade_por_extenso = $e->extenso(count($clientes), Extenso::NUMERO_MASCULINO);

require_once __DIR__ . '/../interface/pagina_cliente_lista.php';
