<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // requisição POST, salva os dados e depois
    // carrega para mostrar o resultado
    $dados_novos = [
        'nome' => $_REQUEST['nome'],
        'telefone' => $_REQUEST['telefone']
    ];
    $codigo_novo_cliente = $repoClientes->inserir($dados_novos);
    
    header('Location: index.php?acao=cliente_lista');
    
    //header("Location: cliente_editar.php?codigo=$codigo_novo_cliente");
} else {
    require_once __DIR__ . '/../interface/pagina_cliente_inserir.php';
}
