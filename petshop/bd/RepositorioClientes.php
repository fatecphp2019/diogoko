<?php

namespace BancoDados;

// padrão Repositório
class RepositorioClientes {
    private $conexao;
    
    public function __construct() {
        $conexao = new \PDO('mysql:dbname=petshop;host=127.0.0.1;charset=UTF8',
            'usu_petshop', '123456');
        $conexao->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $conexao->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        
        $this->conexao = $conexao;
    }
    
    public function listar_clientes($nome_pesquisa) {
        $consulta = $this->conexao->prepare("
    select * from clientes
    where nome like ?
    order by nome
    ");

        $consulta->execute(['%' . $nome_pesquisa . '%']);

        return $consulta->fetchAll();
    }

    public function buscar_por_codigo($codigo) {
        $consulta = $this->conexao->prepare("
select * from clientes where codigo = :codigo
");
        
        $consulta->execute(['codigo' => $codigo]);
        
        return $consulta->fetch();
    }
    
    public function alterar($cliente) {
        $consulta = $this->conexao->prepare("
update clientes
set
    nome = :nome,
    telefone = :telefone
where codigo = :codigo
");
        
        $consulta->execute($cliente);
    }
    
    public function inserir($cliente) {
        $sql = "
insert into clientes
    (nome, telefone)
values
    (:nome, :telefone)
";
        
        $consulta = $this->conexao->prepare($sql);

        $consulta->execute($cliente);
        
        $codigo = $this->conexao->lastInsertId();
        return $codigo;
    }
}
