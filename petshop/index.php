<?php

require_once __DIR__ . '/vendor/autoload.php';

session_start([
    'cookie_lifetime' => 86400,
]);

use BancoDados\RepositorioClientes;
$repoClientes = new RepositorioClientes();

$acao = $_REQUEST['acao'] ?? 'login';

if ($acao == 'cliente_lista') {
    require_once __DIR__ . '/controladores/cliente_lista.php';
} else if ($acao == 'cliente_editar') {
    require_once __DIR__ . '/controladores/cliente_editar.php';
} else if ($acao == 'cliente_inserir') {
    require_once __DIR__ . '/controladores/cliente_inserir.php';
} else if ($acao == 'login') {
    require_once __DIR__ . '/controladores/login.php';
} else if ($acao == 'logout') {
    require_once __DIR__ . '/controladores/logout.php';
}