<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Petshop</title>
        <style>
            body {
                font-family: Verdana, sans-serif;
                font-size: 16px;
                color: #333;
            }
        </style>
    </head>
    <body>
        <h1>Petshop</h1>
        
        <p>Usuário logado: <?= $_SESSION['usuario_logado'] ?></p>
        
        <?php require_once __DIR__ . '/menu.php'; ?>
