<?php require_once __DIR__ . '/cabecalho.php'; ?>

        <h2>Editar cliente</h2>
        
        <form method="POST">
            <div>
                <label>Nome</label>
                <input type="text" name="nome"
                       value="<?= $cliente['nome'] ?>"/>
            </div>
            
            <div>
                <label>Telefone</label>
                <input type="text" name="telefone"
                       value="<?= $cliente['telefone'] ?>"/>
            </div>
            
            <input type="submit" value="Salvar"/>
        </form>
        
<?php require_once __DIR__ . '/rodape.php'; ?>