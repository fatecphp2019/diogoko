<?php require_once __DIR__ . '/cabecalho.php'; ?>

        <h2>Clientes</h2>
        
        <form>
            <label>Nome</label>
            <input type="text" name="nome"
                   value="<?= $nome_pesquisa ?>"/>
            <input type="submit" value="Pesquisar"/>
        </form>
        
        <br>
        
        <p>Existem <?= $quantidade_por_extenso ?> clientes cadastrados.</p>
        
        <br>
        
        <table>
            <tr>
                <th>Nome</th>
                <th>Telefone</th>
            </tr>
            
            <?php foreach ($clientes as $c) { ?>
            <tr>
                <td>
                    <a href="index.php?acao=cliente_editar&codigo=<?= $c['codigo'] ?>">
                        <?= htmlspecialchars($c['nome']) ?>
                    </a>
                </td>
                <td><?= $c['telefone'] ?></td>
            </tr>
            <?php } ?>
        </table>
        
<?php require_once __DIR__ . '/rodape.php'; ?>