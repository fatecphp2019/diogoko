<?php require_once __DIR__ . '/cabecalho.php'; ?>

        <h2>Login</h2>

        <?php if (isset($mensagemErro)): ?>
        <div style="color: red; background: #ffcccc; border: 5px solid red; font-size: xx-large;">
            <?= $mensagemErro ?>
        </div>
        <?php endif; ?>
        
        <form method="POST">
            <div>
                <label>Nome de usuário</label>
                <input type="text" name="nome_usuario"/>
            </div>
            
            <div>
                <label>Senha</label>
                <input type="text" name="senha"/>
            </div>
            
            <input type="submit" value="Login"/>
        </form>
        
<?php require_once __DIR__ . '/rodape.php'; ?>
